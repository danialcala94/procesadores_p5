/*  Autores:  ALCAL� VALERA, DANIEL | CRESPO TORBADO, BEATRIZ | HERRERAS G�MEZ, FRANCISCO
	Practica: Practica  Analizador Sint�ctico LALR con CUP
*/

package es.unileon.procesadores.makers;

import java.io.*; 
import java.nio.file.Files; 
import java.nio.file.*; 

public class CupMaker {
	
	/**
	 * Generates a parser from the CUP file.
	 */
	public static void generateCup() {
		try {
			// "-jar cup-11b.jar -interface -parser Parser calc.cup"
			String arg[] = {"-interface", "-parser", "Parser", "files/calc.cup"};
			java_cup.Main.main(arg);
			moveFiles();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Moves the generated files to the neccesary package.
	 * @throws IOException
	 */
	private static void moveFiles() throws IOException {
        Path temp = Files.move 
        (Paths.get("sym.java"),  
        Paths.get("src/es/unileon/procesadores/parsers/sym.java")); 
  
        if (temp != null) { 
            System.out.println("File renamed and moved successfully"); 
        } else { 
            System.out.println("Failed to move the file"); 
        }
        
        temp = Files.move 
        (Paths.get("Parser.java"),  
        Paths.get("src/es/unileon/procesadores/parsers/Parser.java")); 
  
        if (temp != null) { 
            System.out.println("File renamed and moved successfully"); 
        } else { 
            System.out.println("Failed to move the file"); 
        } 
	}
}
