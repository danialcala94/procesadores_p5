/*  Autores:  ALCAL� VALERA, DANIEL | CRESPO TORBADO, BEATRIZ | HERRERAS G�MEZ, FRANCISCO
	Practica: Practica  Analizador Sint�ctico LALR con CUP
*/

package es.unileon.procesadores.makers;

public class JFlexMaker {
	/**
	 * Generates the parser from the JFlex file.
	 * @param path
	 * @param outputDir
	 */
	public static void generateParser(String path, String outputDir) {
		String args[] = {path, "-d", outputDir};
		jflex.Main.main(args);
	}
}
