package es.unileon.procesadores.parsers;

/*  Autores:  ALCAL� VALERA, DANIEL | CRESPO TORBADO, BEATRIZ | HERRERAS G�MEZ, FRANCISCO
	Practica: Practica  Analizador Sint�ctico LALR con CUP
*/

import java_cup.runtime.*;
%%
%class Yylex
%unicode
%cup
%line
%column
%public
%%
"+" { return new Symbol(sym.MAS); }
"-" { return new Symbol(sym.MENOS); }
"*" { return new Symbol(sym.POR); }
"/" { return new Symbol(sym.ENTRE); }
"%" { return new Symbol(sym.MODULO); }
"^" { return new Symbol(sym.POTENCIA); }
";" { return new Symbol(sym.PTOYCOMA); }
"(" { return new Symbol(sym.ABREPAR); }
")" { return new Symbol(sym.CIERRAPAR); }
[:digit:]+ { return new Symbol(sym.NUM, new Integer(yytext())); }
[ \t\r\n]+ {;}
. { System.out.println("Error en l�xico."+yytext()+"-"); }